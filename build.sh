#!/bin/bash
PATH_TO_FX=/lib/javafx-sdk-16/lib

if [ -e $PATH_TO_FX ]; then
	export PATH_TO_FX
else
	echo "ce fichier $PATH_TO_FX n'existe pas"
	echo "Vous devez mettre le chemin de la bibliothèque JavaFX dans ce script"
fi

find -name "*.java" -exec javac --module-path $PATH_TO_FX --add-modules javafx.controls,javafx.fxml -cp src/main/java "{}" -d out/ \;
cp -r ressources/* out/
