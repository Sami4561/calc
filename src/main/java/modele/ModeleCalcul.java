package modele;

import static java.lang.Math.*;

/**
 * Réalise les opérations de calcul, binaire et unitaire
 */
public class ModeleCalcul {

    /**
     * Permet de recevoir les paramètres de calcul et d'en revoyer la valeur
     * @param nombre1 Nombre positionné devant l'opérateur
     * @param nombre2 Nombre positionné après l'opérateur
     * @param operateur String spécifiant l'opérateur lui-même
     * @return Double Retourne le résultat, pouvant être null
     */
    public Double calculer(double nombre1, double nombre2, String operateur) {
        switch (operateur) {
            case "+":
                return nombre1 + nombre2;
            case "-":
                return nombre1 - nombre2;
            case "*":
                return nombre1 * nombre2;
            case "/":
                if (nombre2 == 0) return null;
                return nombre1 / nombre2;
            case "m":
                int n1 = (int) nombre1;
                int n2 = (int) nombre2;
                if (n2 == 0) return null;
                int mod = floorMod(n1,n2);
                return (double) mod;
            case "^":
                return Math.pow(nombre1,nombre2);
            default:
                throw new IllegalStateException("Unexpected value: " + operateur);
        }
    }

    /**
     * Permet de calculer pour des calcul unitaire
     * @param nombre Nombre après l'opérateur
     * @param operateur L'opérateur lui-même
     * @return Double Retourne le résultat pouvant être null
     */
    public Double calculer(double nombre,String operateur)
    {
        switch (operateur)
        {
            case "√":
                return sqrt(nombre);
            case "cos":
                return cos(nombre);
            case "sin":
                return sin(nombre);
            case "tan":
                return tan(nombre);
        }
        System.err.println("Opérateur inconnu - "+operateur);
        return null;
    }
}
