package launch;

import javafx.stage.Stage;
import vue.VueController;

import java.util.Locale;

/**
 * permet l'initialisation de la calculatrice après le start du programme
 * la localité est set dans cette classe
 */
public class Calculette {
    static VueController controlVue;

    public Calculette(Stage stage)
    {
        // Bien obligé sinon erreur entre la différence de notation du séparateur décimale
        Locale.setDefault(Locale.ENGLISH);
        controlVue = VueController.getInstance();
        controlVue.setWindow(stage);
    }

    /**
     * Méthode pour lancer l'affichage de la calculette
     */
    public void run()
    {
        try{
            //afficher la vue de la calculette
            controlVue.afficherVue();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
