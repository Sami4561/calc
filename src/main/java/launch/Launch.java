package launch;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Comme son nom l'indique elle lance l'application
 * C'est le point de départ du programme
 */
public class Launch extends Application {

    /**
     * La méthode va initialiser une calculatrice en lui passant la fenêtre
     * ensuite elle run la calculette
     * @param stage la fenêtre passé en paramètre
     */
    @Override
    public void start(Stage stage) {
        try {
            Calculette c = new Calculette(stage);
            c.run();
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}

