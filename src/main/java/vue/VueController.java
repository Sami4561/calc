package vue;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.LinkedList;

/**
 * Cette classe permet de Controller la vue de la calculatrice permanente (la choiceBox et le champ de texte)
 * Elle contient la fenêtre et la VBox qui est root de la scene
 */
public class VueController {

    private Stage window = null;
    private static VueController instance;

    private final ChoiceBox<Vue> vueChoiceBox;

    private final VBox box;

    @FXML
    protected Text sortie = new Text();
    @FXML
    public Rectangle rectangleBorder = new Rectangle();

    private VueController() {

        LinkedList<Vue> vueLinkedList = new LinkedList<>();
        try{
            vueLinkedList.add(new VueCalculatriceNormale());
            vueLinkedList.add(new VueCalculatriceScientifique());
        }catch (Exception e)
        {
            System.err.println("[ERREUR] le chargement n'a pas pu se réalisé");
            System.out.println(e.getMessage() + e.getCause());
            System.exit(0xffffffff);
        }
        /* ===== Créer la vue de la calculette qui ne changeras pas ===== */
        box = new VBox();
        box.setAlignment(Pos.TOP_CENTER);

        /* La ChoiceBox */
        vueChoiceBox = new ChoiceBox<>();
        vueChoiceBox.setItems(FXCollections.observableList(vueLinkedList));
        vueChoiceBox.setValue(vueLinkedList.getFirst());
        vueChoiceBox.setOnAction((event) -> actualiserLaVue());
        box.getChildren().add(vueChoiceBox);

        /* ==== Stack Pane ==== */

        /* === Rectangle === */
        rectangleBorder.setStroke(Color.DARKCYAN);
        rectangleBorder.setHeight(50);
        rectangleBorder.setFill(Color.WHITE);
        rectangleBorder.setSmooth(true);
        rectangleBorder.setWidth(270);

        /* === Text === */
        sortie.setFont(new Font("Segoe UI Bold",18));
        StackPane stackPane = new StackPane(rectangleBorder,sortie);
        box.getChildren().add(stackPane);

        /* ===== Listener pour le rectangle ===== */
        sortie.boundsInParentProperty().addListener((obsevable,oldValue,newValue) ->{
                    double width = VueController.getInstance().sortie.getBoundsInParent().getWidth();
                    if (width < 270) width = 270;
                    rectangleBorder.setWidth(width);
                }
        );
    }

    /**
     * Permet d'obtenir l'instance de l'objet
     * @return VueController Retourne l'unique instance de cette classe
     */
    public static VueController getInstance()
    {
        if (instance != null)
            return instance;
        return instance = new VueController();
    }

    public Stage getWindow()
    {
        return window;
    }

    /**
     * Actualise la vue à l'intérieur de la VBox en settant la vue actuellement définie par la choicebox
     */
    public void actualiserLaVue()
    {
        try {
            box.getChildren().set(2, vueChoiceBox.getValue().getParent());
        }catch (IndexOutOfBoundsException index)
        {
            box.getChildren().add(vueChoiceBox.getValue().getParent());
        }
    }

    /**
     * Permet de set les paramètres de la fenêtre de la calculette et de la montrer
     */
    public void afficherVue() {
        var minWidthValue = 400;
        var minHeightScientifiqueValue = 528;

        actualiserLaVue();
        getWindow().setMinHeight(minHeightScientifiqueValue);
        getWindow().setHeight(minHeightScientifiqueValue);
        getWindow().setMinWidth(minWidthValue);
        getWindow().setWidth(minWidthValue);
        getWindow().show();
    }

    /**
     * Setter qui insert la fenêtre dans cette classe
     * @param pStage Le primary stage de Launch dans sa méthode start
     */
    public void setWindow(Stage pStage) {
        window=pStage;
        Scene scene = new Scene(box);
        try {
            scene.getStylesheets().add(getClass().getResource("/DarkTheme.css").toExternalForm());
            box.setId("vboxCalc"); // dans le fichier css le background darktheme est donné au attribut ayant l'id "vboxClac"
        }catch (Exception e)
        {
            System.err.println("SetWindow : "+getClass().getCanonicalName()+" : "+e.getMessage());
        }
        window.setScene(scene);
        window.setTitle("Calculatrice en JavaFX");
        window.show();
    }
}
