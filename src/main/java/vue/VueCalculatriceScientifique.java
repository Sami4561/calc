package vue;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * Une calculatrice scientifique est une calculatrice normale avec la possiblité de réaliser des calculs unitaires
 */
public class VueCalculatriceScientifique extends VueCalculatriceNormale implements Vue{

    /**
     * La HBox contenant quelques opérateurs et le 'e' affichés en haut de la vue normale
     */
    public HBox HboxTOP;

    /**
     * La VBox contenant les opérateurs unaires qui sont affichés à droites de la vue normale
     */
    public VBox VboxRIGHT;

    /**
     * La vue scientifique, permet d'être affiché lorsqu'elle est appelée par la VueController
     */
    private Parent vueScientifique;


    public VueCalculatriceScientifique() {
        super();
        try{
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/CalcScientifique.fxml"));
            fxmlLoader.setController(this);
            fxmlLoader.load();
            HBox hBox = new HBox();
            hBox.setAlignment(Pos.CENTER);
            VBox vBox = new VBox();
            vBox.getChildren().addAll(HboxTOP,super.getParent());
            VboxRIGHT.setAlignment(Pos.CENTER_RIGHT);
            hBox.getChildren().addAll(vBox,VboxRIGHT);
            vueScientifique = hBox;
        }catch (Exception e)
        {
            System.out.println(e.getMessage() + "\n" + e.getCause());
        }
    }

    /**
     * Obtiens le text du bouton cliqué et affecte la variable opérateur de vueCalcNormale
     * @param event Sert à récupérer le text du bouton cliqué
     */
    @FXML
    protected void appuiOperateurUnaire(ActionEvent event)
    {
        if (!getTextAffiche().isEmpty()) return;
        String valeur = ((Button)event.getSource()).getText();
        operateur = valeur;
        setSortie(valeur);
        marche = false;
        unitaire = true;
    }

    /* Ce qui est demandé en implémentant l'interface Vue */
    /**
     * Permet de se définir en tant que Parent visuel
     * @return Parent Retourne le parent qu'il faut affiché pour cette vue
     */
    @Override
    public Parent getParent()
    {
        return vueScientifique;
    }

    /**
     * Ce Override est utile pour que la vue soit lisible dans la choicebox
     * @return String Retourne son identification pour reconnaître la vue
     */
    @Override
    public String toString()
    {
        return "Calculatrice Scientifique";
    }
}
