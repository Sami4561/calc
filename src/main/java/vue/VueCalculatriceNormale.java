package vue;

import javafx.beans.InvalidationListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import modele.ModeleCalcul;

import java.net.URL;
import java.text.DecimalFormat;
import java.util.Objects;
import java.util.ResourceBundle;

/**
 * Cette classe permet de controller ce que possède une calculatrice normale
 * c'est à dire les appuis sur tout les boutons de la vueNormale
 * Cette classe possède deux double et un opérateur pour réaliser les calculs
 */
public class VueCalculatriceNormale implements Initializable,Vue {

    /**
     * La vue normale, permet d'être affiché lorsqu'elle est appelée par la VueController
     */
    private Parent vueNormale;

    private double nombre1;
    private double nombre2;
    /**
     * Ce string contient l'opérateur actuellement utilisé
     */
    protected String operateur = "";

    protected boolean marche = true;

    /**
     * Spécifie si l'opération est unitaire ou non
     */
    protected boolean unitaire = false;

    private final ModeleCalcul modele = new ModeleCalcul();
    private final DecimalFormat df = new DecimalFormat(); // controller la sortie des caractères

    /** Collection observable permettant d'afficher l'historique et de l'actualiser
     * lors de l'utilisation de la calculette
     */
    static final ObservableList<String> Historique = FXCollections.observableArrayList();

    /**
     * Affichage de cette liste dans la fenêtre
     */
    @FXML
    private ListView<String> listView = new ListView<>();

    /** Variable contenant l'expression récupérée à chaque calcul
     */
    private String equation;

    public VueCalculatriceNormale()
    {
        try{
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/CalcNormale.fxml"));
            fxmlLoader.setController(this);
            vueNormale = fxmlLoader.load();
            listView.setMaxWidth(230);
            listView.setItems(Historique);
        }catch (Exception e)
        {
            System.out.println(e.getMessage() + "\n" + e.getCause());
        }
    }

    /* Gestion du Texte en sortie de la calculette */

    /**
     * Ajoute au champ de texte le string donné en paramètre
     * @param s le string qui sera ajouter au texte
     */
    protected void ajouterAuTexte(String s) { VueController.getInstance().sortie.setText(VueController.getInstance().sortie.getText()+s); }

    /**
     * Permet d'obtenir l'instance de texte qui est affiché par la calculatrice
     * @return String Retourne le texte
     */
    protected String getTextAffiche(){ return VueController.getInstance().sortie.getText();}

    /**
     * Permet d'affecter une valeur au texte
     * @param format la valeur qui est affectée au texte
     */
    protected void setSortie(String format) {
        VueController.getInstance().sortie.setText(format);
    }

    /* Gestion des événements sur la vue FXML */

    /**
     * Événement lorsque l'utilisateur appui sur un le bouton "point"
     * ajoute le point au texte
     */
    @FXML
    void appuiPoint()
    {
        if (!marche) ajouterAuTexte(".");
    }

    /**
     * Événement lorsque l'utilisateur appui sur un chiffre
     * ajoute le chiffre au texte
     * @param event la source de l'événement pour connaître le chiffre
     */
    @FXML
    protected void appuiChiffre(ActionEvent event) {
        if (marche) {
            setSortie("");
            marche = false;
        }
        String value = ((Button)event.getSource()).getText();

        if (value.equals("e")) value = "2.7182818284";

        ajouterAuTexte(df.format((Double.parseDouble(value))));
    }

    /**
     * Action réalisée quand l'utilisateur appui sur le bouton égal
     * la calculette lance la transformation du texte pour appeler le modele de calcul
     */
    @FXML
    protected void appuiEgal()
    {
        int lengthSortie = getTextAffiche().length(), indexOperande;
        if (lengthSortie == 0 || operateur.isEmpty()) return;
        indexOperande = obtenirIndiceOperateur();
        // On a trouvé l'opérateur et son indice, il n'y a plus qu'à calculer

        if (!unitaire)
        {
            if (indexOperande > 0){
                try{
                    nombre1 = Double.parseDouble(getTextAffiche().substring(0, indexOperande));
                    nombre2 = Double.parseDouble(getTextAffiche().substring(indexOperande+1, lengthSortie));
                    Double resultat = modele.calculer(nombre1,nombre2,operateur);
                    equation = nombre1 + operateur +nombre2 + "=" + resultat;
                    if (resultat == null){ VueController.getInstance().sortie.setText("NaN"); return;}
                    else nombre1 = resultat;
                }catch (Exception e)
                {
                    System.err.println("Message : "+e.getMessage());
                    System.err.println("Cause : "+e.getCause());
                }
            }
        }
        else { // unaire is true
           nombre2 = Double.parseDouble(getTextAffiche().substring(operateur.length()));
           nombre1 = modele.calculer(nombre2,operateur);
           equation = operateur + nombre2 + "=" + nombre1;
        }
        if(!Objects.isNull(equation)){Historique.add(equation); equation = null;} //ajout de l'expression dans l'historique puis réinitialisation
        //finally
        setSortie(df.format(nombre1));
        nombre2 = 0;
        operateur = "";
        marche = false;
    }

    /**
     * Action réalisée quand l'utilsiateur appui sur un opérateur (ici l'opérateur est binaire)
     * s'il est valdie alors on ajoute l'opérateur au texte
     * @param event la source de l'événement qui permet de connaître l'opérateur
     */
    @FXML
    protected void appuiOperateur(ActionEvent event) {
        String valeur = ((Button)event.getSource()).getText();

        //S'il n'y a pas encore de texte affiché ou qu'il y a un nombre1 && un opérateur et que c'est un plus ou un moins
        if ( (valeur.equals("+") || valeur.equals("-")) && (getTextAffiche().isEmpty() || !operateur.isEmpty()) )
        {
            // alors on ajoute l'opérateur au texte
            ajouterAuTexte(valeur);
            marche = false;
            return;
        }
        if(operateur.isEmpty() && !getTextAffiche().isEmpty()) { //S'il n'y a pas encore d'opérateur sélectionné on peut l'ajouter
            operateur = valeur;
            ajouterAuTexte(operateur);
            marche = false;
            unitaire = false;
        }
    }

    /**
     * Permet d'obtenir l'indice de l'opérateur dans le texte
     * @return int Retourne un entier correspondant à l'indice de l'oérateur dans le texte
     */
    private int obtenirIndiceOperateur()
    {
        int indexOperande = 0;
        char[] listOperande = {'+','-','*','/','m','^'};
        for (char c : listOperande) if (getTextAffiche().indexOf(c) != -1) indexOperande = getTextAffiche().indexOf(c);
        return indexOperande;
    }

    /**
     * Supprime le dernier caractère du champ du texte
     */
    @FXML
    protected void appuiSuppr() {
        /* Enlève simplement un caractère du champ sortie */
        int length = getTextAffiche().length();
        if (length > 0)
            setSortie(getTextAffiche().substring(0, length - 1));
    }

    /**
     * Supprime le text actule et réinitialise tout
     */
    @FXML
    protected void appuiSupprTout() {
        nombre1 = 0;
        nombre2 = 0;
        operateur = "";
        marche = true;
        setSortie("");
    }

    /**
     * initialise la classe lorsqu'elle est demandé, initialise le controller de sortie (DecimalFormat)
     * au nombre significatif après la virgule et surtout les enlèves les zeros inutiles après la virgule
     * @param url inutilisé
     * @param resourceBundle inutilisé
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        df.setMaximumFractionDigits(3); //Set le maximum de chiffre après la virgule
        df.setMinimumFractionDigits(0); //Set le minimum de chiffre après la virgule (0 signifie que la virgule n'apparaît pas pour des 0 inutiles)
        df.setGroupingUsed(false); //Ne sépare pas les milliers par des groupes de 3 chiffres
    }

    /* Ce qui est demandé en implémentant l'interface Vue */

    /**
     * Permet de se définir en tant que Parent visuel
     * @return Parent Retourne le parent qu'il faut affiché pour cette vue
     */
    public Parent getParent() { return vueNormale; }

    /**
     * Ce Override est utile pour que la vue soit lisible dans la choicebox
     * @return String Retourne son identification pour reconnaître la vue
     */
    @Override
    public String toString() { return "Calculatrice normale"; }
}
