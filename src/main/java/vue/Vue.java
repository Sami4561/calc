package vue;

import javafx.scene.Parent;

/**
 * Interface qui oblige les Vues à pouvoir se définir en tant que parent et en tant que String
 */
public interface Vue {
    /**
     * Permet d'obtenir le parent de la vue implémentée pour pouvoir l'affiché à l'écran
     * @return Parent Retourne la vue
     */
    Parent getParent();

    /**
     * Permet de se définir en tant que string lisible pour un utilisateur dans la choicebox
     * @return String Retourne son string qui permet de l'identifié par rapport aux autres vues
     */
    @Override
    String toString();
}
